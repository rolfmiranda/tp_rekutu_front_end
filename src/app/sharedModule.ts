/*
@NgModule({
    imports: [CommonModule],
    exports: [SharedPipe, SharedDirective, SharedComponent]
})*/

import {HoraPipe} from './pipes/hora';
import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {BuscadorConceptoUsoComponent} from './components/dialogBuscador/buscador-conceptoUso/buscador-conceptoUso.component';
import {BuscadorClienteComponent} from './components/dialogBuscador/buscador-cliente/buscador-cliente.component';
import {MatIconModule} from '@angular/material';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MaterialModule} from './app.module';
import {MatSelectFilterModule} from 'mat-select-filter';

@NgModule({
    imports: [
        CommonModule,
        MatIconModule,
        FormsModule,
        MaterialModule,
        ReactiveFormsModule,

    ],
    declarations:[
        HoraPipe,
        BuscadorConceptoUsoComponent,
        BuscadorClienteComponent],
    exports: [
        HoraPipe,
        BuscadorConceptoUsoComponent,
        BuscadorClienteComponent]
})
export class SharedModule{
    static forRoot() {
        return {
            ngModule: SharedModule,
            providers: [ //services that you want to share across modules

            ]
        }
    }
}
