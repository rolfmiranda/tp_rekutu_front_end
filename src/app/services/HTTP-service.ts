import {Injectable} from '@angular/core';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {HttpHeaders} from '@angular/common/http';

import {Observable, of, throwError} from 'rxjs';
import {catchError, map, tap} from 'rxjs/operators';
import {UrlParamBuildingService} from './URLParamsHandlers/urlParamBuilding-service';


@Injectable({
    providedIn: 'root'
})

export class HTTPService {
    private usuarioActual: any;
    private httpOptions={};


    private routes: Map<string, string>;
    private selectedRoute = '';

    private baseURL = 'http://gy7228.myfoscam.org:8080/';
    private ruta = '';

    constructor(private http: HttpClient,
                private urlParamBuildingService: UrlParamBuildingService) {

        this.initCurrentUser();
        this.routes = new Map();
        //rutas de la api

    }

    initCurrentUser(){
        if (JSON.parse(localStorage.getItem('currentUser')))
            this.usuarioActual=JSON.parse(localStorage.getItem('currentUser'))['usuarioLogin'];
        if(this.usuarioActual){
            this.httpOptions = {
                headers: new HttpHeaders(
                    {
                        'Content-Type': 'application/json',
                        'usuario': this.usuarioActual
                    })

            };
        }else{
            this.httpOptions = {
                headers: new HttpHeaders(
                    {
                        'Content-Type': 'application/json'
                    })

            };
        }

    }
    add(e): Observable<any> {
        return this.http.post(this.ruta, e).pipe(catchError(err => {
            return throwError(err);
        }));
    }

    addFile(e): Observable<any> {

        let customHttpOptions={
            reportProgress: true,
            observe: 'events',
            headers: new HttpHeaders({})


        };
        return this.http.post<any>(this.ruta, e, {
            reportProgress: true,
            observe: 'events',
            headers: new HttpHeaders({})
        }).pipe(catchError(err => {
            return throwError(err);
        }));
    }

    getAll(options?: any): Observable<any[]> {
        let opciones = Object.assign({}, this.httpOptions);
        //obtener parametros para consulta  de filtrado por GET
        opciones['params'] = this.urlParamBuildingService.buildParams(options);
        return this.http.get<any[]>(this.ruta, opciones)
            .pipe(catchError(err => {
                return throwError(err);
            }));
    }

    getById(id: string): Observable<any> {
        return this.http.get<any>(this.ruta + id)
            .pipe(catchError(err => {
                return throwError(err);
            }));
    }

    update(e): Observable<any> {
        return this.http.put(this.ruta, e, this.httpOptions)
            .pipe(catchError(err => {
                return throwError(err);
            }));
    }

    delete(id: string): Observable<any> {
        return this.http.delete(this.ruta + id, this.httpOptions)
            .pipe(catchError(err => {
                return throwError(err);
            }));
    }

    setRuta(path: string) {
            this.ruta = this.baseURL + path;
    }

    //Objeto de ejemplo: formato para opciones
    /*let obj= {
        paths:[
            {
                path:'persona',
                id:1
            },
            {
                path:'agenda',
                params:[
                    {
                        param:'fecha',
                        value:'2019-03-05'
                    },
                    {
                        param:'disponible',
                        value: 'S'
                    }
                ]
            }

        ]

    };

     */
    getCustomURL(opciones: any): Observable<any[]> {


        let obj = opciones;
        //fin objeto
        let ruta = '';
        for (let i = 0; i < obj.paths.length; i++) {
            let pathActual = obj.paths[i];
            ruta = ruta + pathActual.path;
            if (pathActual.id) {
                ruta = ruta + '/' + pathActual.id;
            }
            if (pathActual.params) {
                ruta = ruta + '?';
                for (let j = 0; j < pathActual.params.length; j++) {
                    let paramActual = pathActual.params[j];
                    ruta = ruta + paramActual.param + '=' + paramActual.value;
                    if (j + 1 < pathActual.params.length) {
                        ruta = ruta + '&';
                    }
                }
            } else {
                ruta = ruta + '/';
            }

        }
        ruta = this.baseURL + ruta;

        return this.http.get<any[]>(ruta, this.httpOptions)
            .pipe(catchError(err => {
                return throwError(err);
            }));
    }

    addCustomURL(opciones: any, e): Observable<any> {


        let obj = opciones;
        //fin objeto
        let ruta = '';
        for (let i = 0; i < obj.paths.length; i++) {
            let pathActual = obj.paths[i];
            ruta = ruta + pathActual.path;
            if (pathActual.id) {
                ruta = ruta + '/' + pathActual.id;
            }
            ruta = ruta + '/';

        }
        ruta = this.baseURL + ruta;

        return this.http.post(ruta, e, this.httpOptions)
            .pipe(catchError(err => {
                return throwError(err);
            }));
    }

    deleteCustomURL(opciones: any): Observable<any> {


        let obj = opciones;
        //fin objeto
        let ruta = '';
        for (let i = 0; i < obj.paths.length; i++) {
            let pathActual = obj.paths[i];
            ruta = ruta + pathActual.path;
            if (pathActual.id) {
                ruta = ruta + '/' + pathActual.id;
            }
            ruta = ruta + '/';

        }
        ruta = this.baseURL + ruta;

        return this.http.delete(ruta, this.httpOptions)
            .pipe(catchError(err => {
                return throwError(err);
            }));
    }


}
