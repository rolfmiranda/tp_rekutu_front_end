import {Injectable} from '@angular/core';
import {UrlParamsModel} from '../../models/urlParams.model';
import {HttpParams} from '@angular/common/http';
import {PaginationService} from './pagination-service';
import {SortingService} from './sorting-service';
import {FilteringService} from './filtering-service';


@Injectable()

export class UrlParamBuildingService {
    private urlParamsModel: UrlParamsModel;

    constructor(
        private paginationService: PaginationService,
        private sortingService: SortingService,
        private filteringService: FilteringService,
    ) {
        this.urlParamsModel = new UrlParamsModel();
    }

    buildParams(options?: any): HttpParams {
        let isPaginated: boolean = false;
        let isFiltered: boolean = false;
        let isSorted: boolean = false;

        if (options) {
            if (options['UrlParams']) {
                let opt: any[];
                opt = options['UrlParams'];
                for (let i = 0; i < opt.length; i++) {
                    switch (opt[i]) {
                        case 'pagination': {
                            isPaginated = true;
                            break;
                        }
                        case 'filter': {
                            isFiltered = true;
                            break;
                        }
                        case 'sort': {
                            isSorted = true;
                            break;
                        }
                        default: {
                            console.log('ERROR de parametro en servicio HTTP: La opción \'' + opt[i] + '\' no es válida');
                        }
                    }
                }
            }

            if (options['resetParams']) {
                let opt: any[];
                opt = options['resetParams'];
                for (let i = 0; i < opt.length; i++) {
                    switch (opt[i]) {
                        case 'pagination': {
                            this.paginationService.reset();
                            break;
                        }
                        case 'filter': {
                            this.filteringService.reset();
                            break;
                        }
                        case 'sort': {
                            this.sortingService.reset();
                            break;
                        }
                        default: {
                            console.log('ERROR en reinicio de Parametro: La opción \'' + opt[i] + '\' no es válida');
                        }
                    }
                }
            }
        }

        //limpiar datos anteriores
        this.reset();

        //parametros para paginacion server side
        if (isPaginated) {
            this.setParams('inicio', String(this.paginationService.page * this.paginationService.pageSize));
            this.setParams('cantidad', String(this.paginationService.pageSize));

        } else {
            this.paginationService.reset();
        }
        //verificar si se pidió ordenar por alguna columna para ordenacion server side
        if (isSorted) {
            if (this.sortingService.active && this.sortingService.direction !== '') {
                this.setParams('orderBy', this.sortingService.active);
                this.setParams('orderDir', this.sortingService.direction);
            }
        }
        /*
        else {
            this.sortingService.reset();
        }

         */
        //verificar si se pidio filtrar por objecto y/o por coincidencia (like)
        if (isFiltered) {
            let queryParam = this.filteringService.queryParam;
            if (Object.keys(queryParam).length > 0) {
                for (let queryParamKey in queryParam) {
                    this.setParams(queryParamKey, queryParam[queryParamKey]);
                }
            }
            let searchParam = this.filteringService.searchParam;
            if (Object.keys(searchParam).length > 0) {
                this.setParams('ejemplo', JSON.stringify(searchParam));

                if (this.filteringService.like === 'S') {
                    this.setParams('like', 'S');
                }
            }
        } else {
            //restablecer datos de filtro
            this.filteringService.reset();
        }

        return this.urlParamsModel.params;
    }

    //recibir datos para formar parametro '&param=value'
    setParams(param, value) {
        this.urlParamsModel.params = this.urlParamsModel.params.append(param, value);
    }

    reset() {
        this.urlParamsModel = new UrlParamsModel();
    }


}
