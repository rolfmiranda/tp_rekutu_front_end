import {Injectable} from '@angular/core';
import {FilterModel} from '../../models/filter.model';


@Injectable()

export class FilteringService {
    private filterModel: FilterModel;

    get searchParam(): any {
        return this.filterModel.searchParam;
    }

    get like(): string {
        return this.filterModel.like;
    }
    get queryParam(): any {
        return this.filterModel.queryParam;
    }

    constructor() {
        this.filterModel = new FilterModel();
    }

    changesearchParam(searchParam: any, isLike: boolean) {

        this.filterModel.searchParam = searchParam;

        if (isLike) {
            this.filterModel.like = 'S';
        } else {
            this.filterModel.like = 'N';
        }
    }
    addSearchParam(searchParam: any, isLike: boolean){
        for (let searchParamKey in searchParam) {
            this.filterModel.searchParam[searchParamKey]=searchParam[searchParamKey];
        }
        if (isLike) {
            this.filterModel.like = 'S';
        } else {
            this.filterModel.like = 'N';
        }
    }
    addQueryParam(queryParam){
        for (let queryParamKey in queryParam) {
            this.filterModel.queryParam[queryParamKey]=queryParam[queryParamKey];
        }
    }
    reset() {
        this.filterModel = new FilterModel();

    }

    destroySearchParam(paramObjectField: string) {
        let obj = {};
        for (let searchParamKey in this.filterModel.searchParam) {
            if (searchParamKey !== paramObjectField) {
                obj[searchParamKey] = this.filterModel.searchParam[searchParamKey];
            }
        }
        this.filterModel.searchParam = obj;
        this.filterModel.like = 'N';
    }

}
