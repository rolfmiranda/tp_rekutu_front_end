import {Injectable} from '@angular/core';

import {PaginationModel} from '../../models/pagination.model';
import {PageEvent, Sort} from '@angular/material';
import {SortModel} from '../../models/sort.model';


@Injectable()

export class SortingService {
    private sortModel: SortModel;
    get active(): string {
        return this.sortModel.active;
    }

    get direction(): string {
        return this.sortModel.direction;
    }



    constructor() {
        this.sortModel = new SortModel();
    }

    change($event: Sort) {
        this.sortModel.active = $event.active;
        this.sortModel.direction = $event.direction;
    }

    reset() {
        this.sortModel= new SortModel();
    }
}
