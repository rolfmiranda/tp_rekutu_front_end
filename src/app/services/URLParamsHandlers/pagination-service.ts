import {Injectable} from '@angular/core';

import {PaginationModel} from '../../models/pagination.model';
import {PageEvent} from '@angular/material';


@Injectable()

export class PaginationService {
    private paginationModel: PaginationModel;
    get page(): number {
        return this.paginationModel.pageIndex;
    }

    get selectItemsPerPage(): number[] {
        return this.paginationModel.selectItemsPerPage;
    }

    get pageSize(): number {
        return this.paginationModel.pageSize;
    }

    constructor() {
        this.paginationModel = new PaginationModel();
    }

    change(pageEvent: PageEvent) {
        this.paginationModel.pageIndex = pageEvent.pageIndex;
        this.paginationModel.pageSize = pageEvent.pageSize;
        this.paginationModel.allItemsLength = pageEvent.length;
    }
    goTo(tipoPaginacion: string){
        if(tipoPaginacion==='anterior'){
            this.paginationModel.pageIndex= this.paginationModel.pageIndex-1;

        }
        else if(tipoPaginacion ==='siguiente'){
            this.paginationModel.pageIndex= this.paginationModel.pageIndex+1;
        }
    }

    hasPrevious(){
        return this.paginationModel.pageIndex>0;
    }
    reset(){
        this.paginationModel = new PaginationModel();
    }
}
