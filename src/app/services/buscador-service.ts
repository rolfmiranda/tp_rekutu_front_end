import {Injectable} from '@angular/core';

@Injectable({
    providedIn: 'root'
})

export class BuscadorService {
   private conceptoUso:any;
   private cliente: any;
   private required=false;


    reset(){
        this.conceptoUso= null;
        this.cliente= null;
    }

    setConceptoUso(conceptoUso){
        this.conceptoUso=conceptoUso;
    }
    setCliente(cliente){
        this.cliente=cliente;
    }

    get getConceptoUso(): any {
        return this.conceptoUso;
    }

    get getCliente(): any {
        return this.cliente;
    }
    isRequired(): boolean{
        return this.required;
    }
    setRequired(value: boolean){
        this.required=value;
    }

}
