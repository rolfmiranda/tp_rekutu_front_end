import {ModuleWithProviders, NgModule} from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule } from '@angular/router';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { APP_BASE_HREF } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import {
    MatAutocompleteModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatCardModule,
    MatCheckboxModule,
    MatChipsModule,
    MatDialogModule,
    MatExpansionModule,
    MatGridListModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatMenuModule,
    MatNativeDateModule,
    MatPaginatorModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    MatRippleModule,
    MatSelectModule,
    MatSidenavModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatSnackBarModule,
    MatSortModule,
    MatTableModule,
    MatTabsModule,
    MatToolbarModule,
    MatTooltipModule,
    MatStepperModule, MAT_DATE_LOCALE, MAT_DATE_FORMATS, DateAdapter,
} from '@angular/material';
import { MatDatepickerModule } from '@angular/material/datepicker';

import { AppComponent } from './app.component';

import { SidebarModule } from './components/template/sidebar/sidebar.module';
import { FooterModule } from './components/template/shared/footer/footer.module';
import { NavbarModule} from './components/template/shared/navbar/navbar.module';
import { FixedpluginModule} from './components/template/shared/fixedplugin/fixedplugin.module';
import { AdminLayoutComponent } from './components/template/layouts/admin/admin-layout.component';
import { AuthLayoutComponent } from './components/template/layouts/auth/auth-layout.component';

import { AppRoutes } from './app.routing';
import {PaginationService} from './services/URLParamsHandlers/pagination-service';
import {SortingService} from './services/URLParamsHandlers/sorting-service';
import {FilteringService} from './services/URLParamsHandlers/filtering-service';
import {UrlParamBuildingService} from './services/URLParamsHandlers/urlParamBuilding-service';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import {NgxMaterialTimepickerModule} from 'ngx-material-timepicker';

import {HTTPService} from './services/HTTP-service';
import {ExcelService} from './services/excel-service';


@NgModule({
  exports: [
    MatAutocompleteModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatCardModule,
    MatCheckboxModule,
    MatChipsModule,
    MatStepperModule,
    MatDatepickerModule,
    MatDialogModule,
    MatExpansionModule,
    MatGridListModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatMenuModule,
    MatNativeDateModule,
    MatPaginatorModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    MatRippleModule,
    MatSelectModule,
    MatSidenavModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatSnackBarModule,
    MatSortModule,
    MatTableModule,
    MatTabsModule,
    MatToolbarModule,
    MatTooltipModule,
      NgxMaterialTimepickerModule
  ],
  imports: [ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production })]
})
export class MaterialModule {}

@NgModule({
    imports:      [
        CommonModule,
        BrowserAnimationsModule,
        FormsModule,
        RouterModule.forRoot(AppRoutes,{
          useHash: true
        }),
        HttpModule,
        HttpClientModule,
        MaterialModule,
        MatNativeDateModule,
        MatDatepickerModule,
        SidebarModule,
        NavbarModule,
        FooterModule,
        FixedpluginModule,
        NgxMaterialTimepickerModule,
        MatDialogModule

    ],
    declarations: [
        AppComponent,
        AdminLayoutComponent,
        AuthLayoutComponent
    ],
    providers: [
        PaginationService,
        SortingService,
        FilteringService,
        UrlParamBuildingService,
        HTTPService,
        ExcelService
    ],
    bootstrap:    [ AppComponent ]
})
export class AppModule {
}
