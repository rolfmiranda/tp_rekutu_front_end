import {HttpParams} from '@angular/common/http';

export class UrlParamsModel {
    //Constructor de parámetros URL para request por GET Http
    params: HttpParams;

    constructor() {
        this.params = new HttpParams();
    }


}
