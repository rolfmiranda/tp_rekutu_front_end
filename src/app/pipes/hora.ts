import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'hora' })
export class HoraPipe implements PipeTransform {
    transform(horaCadena: string) {

        return horaCadena.substr(0,2)+':'+horaCadena.substr(2,2);
    }
}
