import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'dia' })
export class DiaPipe implements PipeTransform {
    transform(numeroDia: number) {
        let dias=['Domingo','Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'];
        return dias[numeroDia];
    }
}
