import {Routes} from '@angular/router';

import {ListaClientesComponent} from './lista-clientes/lista-clientes.component';
import {NuevoClienteComponent} from './nuevo-cliente/nuevo-cliente.component';
import {NuevoCargarPuntosComponent} from './cargarPuntos/nuevo-cargarPuntos.component';
import {NuevoUsarPuntosComponent} from './usarPuntos/nuevo-usarPuntos.component';


export const ClientesRoutes: Routes = [
    {
        path: '',
        children: [
            {
                path: '',
                redirectTo: 'lista'
            },
            {
                path: 'lista',
                component: ListaClientesComponent
            },
            {
                path: 'nuevo',
                component: NuevoClienteComponent
            },
            {
                path: 'cargar_puntos',
                component: NuevoCargarPuntosComponent
            },
            {
                path: 'usar_puntos',
                component: NuevoUsarPuntosComponent
            },
        ]
    }
];
