import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {MaterialModule} from '../../app.module';

import {MatSelectFilterModule} from 'mat-select-filter';
import {ClientesRoutes} from './clientes.routing';
import {ListaClientesComponent} from './lista-clientes/lista-clientes.component';
import {NuevoClienteComponent} from './nuevo-cliente/nuevo-cliente.component';
import {NuevoCargarPuntosComponent} from './cargarPuntos/nuevo-cargarPuntos.component';
import {NuevoUsarPuntosComponent} from './usarPuntos/nuevo-usarPuntos.component';
import {BuscadorClienteComponent} from '../dialogBuscador/buscador-cliente/buscador-cliente.component';
import {BuscadorConceptoUsoComponent} from '../dialogBuscador/buscador-conceptoUso/buscador-conceptoUso.component';
import {SharedModule} from '../../sharedModule';


@NgModule({
    imports: [
	//cambios para Sistemas Ditribuidos 2020
        CommonModule,
        RouterModule.forChild(ClientesRoutes),
        FormsModule,
        MaterialModule,
        MatSelectFilterModule,
        SharedModule.forRoot(),

    ],
    declarations: [
        ListaClientesComponent,
        NuevoClienteComponent,
        NuevoCargarPuntosComponent,
        NuevoUsarPuntosComponent
    ],
    entryComponents: [
        BuscadorClienteComponent,
        BuscadorConceptoUsoComponent
    ],
})

export class ClientesModule {
}
