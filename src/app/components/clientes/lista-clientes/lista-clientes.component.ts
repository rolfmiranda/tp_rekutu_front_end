import {Component, OnInit} from '@angular/core';

import {PaginationService} from '../../../services/URLParamsHandlers/pagination-service';
import {SortingService} from '../../../services/URLParamsHandlers/sorting-service';
import {FilteringService} from '../../../services/URLParamsHandlers/filtering-service';
import swal from 'sweetalert2';
import {HTTPService} from '../../../services/HTTP-service';

import {Notificador} from '../../../functions/notifications';

@Component({
    selector: 'app-pacientes',
    templateUrl: './lista-clientes.component.html'
})

export class ListaClientesComponent implements OnInit {
    dataSource = [];
    nombreSelected = '';
    constructor(public httpService: HTTPService,
                public paginationService: PaginationService) {
    }


    ngOnInit() {
        this.paginationService.reset();
        this.loadData({
            startIndexPage: this.paginationService.page* this.paginationService.pageSize,
            pageSize: this.paginationService.pageSize
        },
            {
                UrlParams: ['pagination'],
                resetParams: ['pagination']
            });
    }

    loadData( pagination: any,options?: any) {
        this.httpService.setRuta('rest/clientes/getByPage?parametro='+this.nombreSelected);
        this.httpService.add(pagination)
            .subscribe(e => {
                    this.dataSource = e['data']['clientes'];
                },
                err => {
                    console.log(err);
                    err['customMensaje'] = 'No se pudo obtener la lista de clientes.';

                    Notificador.show(err);
                });
    }


    handleFilter() {
        this.paginationService.reset();
        this.loadData({
            startIndexPage: this.paginationService.page* this.paginationService.pageSize,
            pageSize: this.paginationService.pageSize
        },
            {
                UrlParams: ['pagination']
            });
    }

    limpiarFiltroNombre() {
        this.nombreSelected = '';
        this.handleFilter();
    }

    goAnterior() {
        this.paginationService.goTo('anterior');
        this.loadData({
                startIndexPage: this.paginationService.page* this.paginationService.pageSize,
                pageSize: this.paginationService.pageSize
            },
            {
                UrlParams: ['pagination']
            });
    }
    goSiguiente() {
        this.paginationService.goTo('siguiente');
        this.loadData({
                startIndexPage: this.paginationService.page * this.paginationService.pageSize,
                pageSize: this.paginationService.pageSize
            },
            {
                UrlParams: ['pagination']
            });
    }

    goFirst() {
        this.paginationService.reset();
        this.loadData({
                startIndexPage: this.paginationService.page* this.paginationService.pageSize,
                pageSize: this.paginationService.pageSize
            },
            {
                UrlParams: ['pagination']
            });
    }
}


