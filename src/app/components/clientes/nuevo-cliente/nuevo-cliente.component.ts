import { Component, Input, OnInit } from '@angular/core';
import { FilteringService } from '../../../services/URLParamsHandlers/filtering-service';
import { NgForm } from '@angular/forms';
import { Location } from '@angular/common';
import { HTTPService } from '../../../services/HTTP-service';
import { Notificador } from '../../../functions/notifications';
import * as moment from 'moment';
import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE} from '@angular/material';
import {MAT_MOMENT_DATE_FORMATS, MomentDateAdapter} from '@angular/material-moment-adapter';

declare var $: any;

@Component({
    selector: 'app-nuevo-paciente',
    templateUrl: 'nuevo-cliente.component.html',
    providers: [
        {provide: MAT_DATE_LOCALE, useValue: 'es-PY'},
        {provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE]},
        {provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS}
    ]
})

export class NuevoClienteComponent {
    constructor(public httpService: HTTPService,
        public location: Location
    ) {
    }

    onSubmit(form: NgForm) {
        let body = {};

        body['nombre'] = form.value.nombre;
        body['apellido'] = form.value.apellido;
        body['telefono'] = form.value.telefono;
        body['email'] = form.value.email;

        body['nroDocumento'] = form.value.nroDocumento;

        body['fechaNacimiento']=moment(form.value.fechaNacimiento).format('YYYY-MM-DD');

        this.httpService.setRuta('rest/clientes/add');
        this.httpService.add(body)
            .subscribe(e => {
                e['ok'] = true;
                e['customMensaje'] = 'Registro de cliente creado exitosamente';
                Notificador.show(e);
                this.location.back();
            },
                err => {
                    console.log(err);
                    Notificador.show(err);

                });
    }
}
