import {Component} from '@angular/core';
import {NgForm} from '@angular/forms';
import {Location} from '@angular/common';
import {HTTPService} from '../../../services/HTTP-service';
import {Notificador} from '../../../functions/notifications';
import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE, MatDialog} from '@angular/material';
import {MAT_MOMENT_DATE_FORMATS, MomentDateAdapter} from '@angular/material-moment-adapter';
import {BuscadorService} from '../../../services/buscador-service';
import {BuscadorClienteComponent} from '../../dialogBuscador/buscador-cliente/buscador-cliente.component';

@Component({
    selector: 'app-nuevo-paciente',
    templateUrl: 'nuevo-cargarPuntos.component.html',
    providers: [
        {provide: MAT_DATE_LOCALE, useValue: 'es-PY'},
        {provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE]},
        {provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS}
    ]
})

export class NuevoCargarPuntosComponent {
    empleadoActual: any ={nombre:'', apellido: ''};
    constructor(public httpService: HTTPService,
                public location: Location,
                public buscadorService: BuscadorService,
                public dialog: MatDialog,

    ) {
    }

    onSubmit(form: NgForm) {
        let body = {};
        body['clienteId'] =this.empleadoActual.id;
        body['monto'] =form.value.monto;

        this.httpService.setRuta('rest/bolsas/add');
        this.httpService.add(body)
            .subscribe(e => {
                    e['ok'] = true;
                    e['customMensaje'] = 'Punto cargado exitosamente';
                    Notificador.show(e);
                    this.location.back();
                },
                err => {
                    console.log(err);
                    Notificador.show(err);

                });
    }

    dialogFilterEmpleado() {

        this.buscadorService.setRequired(true);
        const dialogRef = this.dialog.open(BuscadorClienteComponent);
        dialogRef.afterClosed().subscribe(result => {
            if(result===true){
                this.empleadoActual=this.buscadorService.getCliente;
                this.buscadorService.reset();
            }
        });
    }

}
