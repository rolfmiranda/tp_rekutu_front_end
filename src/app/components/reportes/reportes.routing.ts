import {Routes} from '@angular/router';
import {ReporteUsoPuntosComponent} from './reporte-usoPuntos/reporte-usoPuntos.component';
import {ReporteBolsaPuntosComponent} from './reporte-bolsaPuntos/reporte-bolsaPuntos.component';
import {ConsultarClientesVencimientoComponent} from './reporte-clientesVencimiento/consultarClientesVencimiento.component';


export const ReportesRoutes: Routes = [
    {
        path: '',
        children: [
            {
                path: '',
                redirectTo: 'puntos_uso'
            },
            {
                path: 'puntos_uso',
                component: ReporteUsoPuntosComponent
            },
            {
                path: 'bolsas_puntos',
                component: ReporteBolsaPuntosComponent
            },
            {
                path: 'clientes_vencimiento',
                component: ConsultarClientesVencimientoComponent
            }

        ]
    }
];
