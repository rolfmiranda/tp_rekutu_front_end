import {Component, OnInit, ViewChild} from '@angular/core';
import {
    DateAdapter,
    MAT_DATE_FORMATS,
    MAT_DATE_LOCALE,
    MatDatepicker, MatDialog,
    MatPaginator,
    MatSort,
    MatTableDataSource,
    PageEvent,
    Sort
} from '@angular/material';
import {PaginationService} from '../../../services/URLParamsHandlers/pagination-service';
import {SortingService} from '../../../services/URLParamsHandlers/sorting-service';
import {FilteringService} from '../../../services/URLParamsHandlers/filtering-service';
import swal from 'sweetalert2';
import * as moment from 'moment';
import {MAT_MOMENT_DATE_FORMATS, MomentDateAdapter} from '@angular/material-moment-adapter';
import {HTTPService} from '../../../services/HTTP-service';
import {Notificador} from '../../../functions/notifications';
import {ExcelService} from '../../../services/excel-service';
import {BuscadorConceptoUsoComponent} from '../../dialogBuscador/buscador-conceptoUso/buscador-conceptoUso.component';
import {BuscadorClienteComponent} from '../../dialogBuscador/buscador-cliente/buscador-cliente.component';
import {BuscadorService} from '../../../services/buscador-service';

declare var $: any;
import * as jspdf from 'jspdf';
import 'jspdf-autotable';

@Component({
    selector: 'app-resumen',
    templateUrl: './reporte-usoPuntos.component.html',
    styleUrls:['reporte-usoPuntos.component.css'],
    providers: [
        {provide: MAT_DATE_LOCALE, useValue: 'es-PY'},
        {provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE]},
        {provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS}
    ]
})

export class ReporteUsoPuntosComponent implements OnInit {
    dataSource=[];
    serviciosParaExcel=[];
    serviciosParaPDF=[];
    clienteSelected:any;
    conceptoSelected: any;

    constructor(private httpService: HTTPService,
                public filteringService: FilteringService,
                public buscadorService: BuscadorService,
                public dialog: MatDialog,
                private excelService: ExcelService) {
    }

    @ViewChild('fechaDesdePicker', {static: true}) fechaDesdeFilter: MatDatepicker<Date>;
    @ViewChild('fechaHastaPicker', {static: true}) fechaHastaFilter: MatDatepicker<Date>;

    ngOnInit() {
        this.clienteSelected='null';
        this.conceptoSelected='null';
        this.loadData({});

    }



    loadData(objParam:any) {
        this.httpService.setRuta('rest/consultas/usoPuntos');
        this.httpService.add(objParam)
            .subscribe(e => {
                    this.dataSource = e['data']['cabeceras'];

                },
                err => {
                    console.log(err);
                    err['customMensaje'] = 'No se pudo obtener la lista de usos de puntos.';
                    Notificador.show(err);

                });
    }

    exportAsXLSX():void {
        //copiar datos a exportar
        let obj={};
        this.serviciosParaExcel=[];
        for(let i=0;i<this.dataSource.length;i++){
            obj['id']=this.dataSource[i].id;
            obj['fecha']=this.dataSource[i].fecha;
            obj['puntajeUtilizado']=this.dataSource[i].puntajeUtilizado;
            obj['vale.descripcion']=this.dataSource[i].vale.descripcion;
            for( let j=0; j<this.dataSource[i].usoDetalle.length;j++){
                obj['detalle.id_' + Number(j+1).toString()] = this.dataSource[i].usoDetalle[j].id;
                obj['detalle.puntajeUtilizado_' + Number(j+1).toString()]=this.dataSource[i].usoDetalle[j].puntajeUtilizado;
                obj['idBolsa_' + Number(j+1).toString()]=this.dataSource[i].usoDetalle[j].puntajeUtilizado;
                obj['bolsa.cliente_' + Number(j+1).toString()]=this.dataSource[i].usoDetalle[j].bolsaPuntos.cliente.nombre + ' ' + this.dataSource[i].usoDetalle[j].bolsaPuntos.cliente.apellido;
                obj['bolsa.estado_' + Number(j+1).toString()]=this.dataSource[i].usoDetalle[j].bolsaPuntos.estado;
                obj['bolsa.fechaAsignacion_' + Number(j+1).toString()]=this.dataSource[i].usoDetalle[j].bolsaPuntos.fechaAsignacion;
                obj['bolsa.fechaCaducidad_' + Number(j+1).toString()]=this.dataSource[i].usoDetalle[j].bolsaPuntos.fechaCaducidad;
                obj['bolsa.monto_' + Number(j+1).toString()] = this.dataSource[i].usoDetalle[j].bolsaPuntos.monto;
                obj['bolsa.puntajeAsignado_' + Number(j+1).toString()] = this.dataSource[i].usoDetalle[j].bolsaPuntos.puntajeAsignado;
                obj['bolsa.puntajeUtilizado_' + Number(j+1).toString()] = this.dataSource[i].usoDetalle[j].bolsaPuntos.puntajeUtilizado;
                obj['bolsa.saldo_' + Number(j+1).toString()] = this.dataSource[i].usoDetalle[j].bolsaPuntos.saldo;
            }
            this.serviciosParaExcel.push(obj);
            obj={};

        }
        this.excelService.exportAsExcelFile(this.serviciosParaExcel, 'Reporte-Usopuntos');
    }

    exportAsPDF(): void{

        var columns = [
            {title: "ID", dataKey: "id"},
            {title: "Fecha", dataKey: "fecha"},
            {title: "Puntaje Utilizado", dataKey: "puntajeUtilizado"},
            {title: "Concepto de Uso", dataKey: "vale.descripcion"}
        ];


        //copiar datos a exportar
        let obj = {};
        this.serviciosParaPDF = [];

        for(let i=0;i<this.dataSource.length;i++){
            obj['id']=this.dataSource[i].id;
            obj['fecha']=this.dataSource[i].fecha;
            obj['puntajeUtilizado']=this.dataSource[i].puntajeUtilizado;
            obj['vale.descripcion']=this.dataSource[i].vale.descripcion;
            for( let j=0; j<this.dataSource[i].usoDetalle.length;j++){
                obj['detalle.id_' + Number(j+1).toString()] = this.dataSource[i].usoDetalle[j].id;
                columns.push({title: "Detalle ID", dataKey: 'detalle.id_' + Number(j+1).toString()});
                obj['detalle.puntajeUtilizado_' + Number(j+1).toString()]=this.dataSource[i].usoDetalle[j].puntajeUtilizado;
                columns.push({title: "Puntaje Utilizado", dataKey: 'detalle.puntajeUtilizado_' + Number(j+1).toString()});
                obj['idBolsa_' + Number(j+1).toString()]=this.dataSource[i].usoDetalle[j].puntajeUtilizado;
                columns.push({title: "Bolsa ID", dataKey: 'idBolsa_' + Number(j+1).toString()});
                obj['bolsa.cliente_' + Number(j+1).toString()]=this.dataSource[i].usoDetalle[j].bolsaPuntos.cliente.nombre + ' ' + this.dataSource[i].usoDetalle[j].bolsaPuntos.cliente.apellido;
                columns.push({title: "Cliente", dataKey: 'bolsa.cliente_' + Number(j+1).toString()});
                obj['bolsa.estado_' + Number(j+1).toString()]=this.dataSource[i].usoDetalle[j].bolsaPuntos.estado;
                columns.push({title: "Estado", dataKey: 'bolsa.estado_' + Number(j+1).toString()});
                obj['bolsa.fechaAsignacion_' + Number(j+1).toString()]=this.dataSource[i].usoDetalle[j].bolsaPuntos.fechaAsignacion;
                columns.push({title: "Fecha de Asignacion", dataKey: 'bolsa.fechaAsignacion_' + Number(j+1).toString()});
                obj['bolsa.fechaCaducidad_' + Number(j+1).toString()]=this.dataSource[i].usoDetalle[j].bolsaPuntos.fechaCaducidad;
                columns.push({title: "Fecha de Caducidad", dataKey: 'bolsa.fechaCaducidad_' + Number(j+1).toString()});
                obj['bolsa.monto_' + Number(j+1).toString()] = this.dataSource[i].usoDetalle[j].bolsaPuntos.monto;
                columns.push({title: "Monto", dataKey: 'bolsa.monto_' + Number(j+1).toString()});
                obj['bolsa.puntajeAsignado_' + Number(j+1).toString()] = this.dataSource[i].usoDetalle[j].bolsaPuntos.puntajeAsignado;
                columns.push({title: "Puntaje Asignado", dataKey: 'bolsa.puntajeAsignado_' + Number(j+1).toString()});
                obj['bolsa.puntajeUtilizado_' + Number(j+1).toString()] = this.dataSource[i].usoDetalle[j].bolsaPuntos.puntajeUtilizado;
                columns.push({title: "Puntaje Utilizado", dataKey: 'bolsa.puntajeUtilizado_' + Number(j+1).toString()});
                obj['bolsa.saldo_' + Number(j+1).toString()] = this.dataSource[i].usoDetalle[j].bolsaPuntos.saldo;
                columns.push({title: "Saldo", dataKey: 'bolsa.saldo_' + Number(j+1).toString()});
            }
            this.serviciosParaPDF.push(obj);
            obj={};

        }
        var doc = new jspdf({
            orientation: 'landscape',
                unit: 'mm',
                format: [8000, 10000]
        });

        doc.autoTable(columns, this.serviciosParaPDF);
        doc.save('Reporte-UsoPuntos.pdf');

    }


    dialogFilterCliente() {
        this.buscadorService.setRequired(false);
        const dialogRef = this.dialog.open(BuscadorClienteComponent);
        dialogRef.afterClosed().subscribe(result => {
            if(result===true){
                this.clienteSelected=this.buscadorService.getCliente;
                this.buscadorService.reset();

                this.handleFilter();
            }
        });
    }
    dialogFilterConceptoUso() {
        this.buscadorService.setRequired(false);
        const dialogRef = this.dialog.open(BuscadorConceptoUsoComponent);
        dialogRef.afterClosed().subscribe(result => {
            if(result===true){
                this.conceptoSelected=this.buscadorService.getConceptoUso;
                this.buscadorService.reset();

                this.handleFilter();
            }
        });
    }
    limpiarFiltroCliente() {
        this.clienteSelected = 'null';
        this.handleFilter();
    }
    limpiarFiltroConceptoUso() {
        this.conceptoSelected = 'null';
        this.handleFilter();
    }
    handleFilter() {
        let objParam={};
        if(this.clienteSelected !=='null'){
            objParam['clienteId']=this.clienteSelected.id;
        }
        if(this.conceptoSelected !== 'null'){
            objParam['valeId']=this.conceptoSelected.id;

        }
        this.loadData(objParam);
    }


}


