import {Component} from '@angular/core';
import {NgForm} from '@angular/forms';
import {Location} from '@angular/common';
import {HTTPService} from '../../../services/HTTP-service';
import {Notificador} from '../../../functions/notifications';
import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE, MatDialog} from '@angular/material';
import {MAT_MOMENT_DATE_FORMATS, MomentDateAdapter} from '@angular/material-moment-adapter';
import {BuscadorService} from '../../../services/buscador-service';
import {BuscadorClienteComponent} from '../../dialogBuscador/buscador-cliente/buscador-cliente.component';
import * as jspdf from 'jspdf';
import 'jspdf-autotable';
import {ExcelService} from '../../../services/excel-service';
@Component({
    selector: 'app-nuevo-paciente',
    templateUrl: 'consultarClientesVencimiento.component.html',
    providers: [
        {provide: MAT_DATE_LOCALE, useValue: 'es-PY'},
        {provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE]},
        {provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS}
    ]
})

export class ConsultarClientesVencimientoComponent {
    clientes=[];
    serviciosParaExcel=[];
    serviciosParaPDF=[];
    mensajeResponse='';
    constructor(public httpService: HTTPService,
                public location: Location,
                public buscadorService: BuscadorService,
                public dialog: MatDialog,
                private excelService: ExcelService
    ) {
    }

    onSubmit(form: NgForm) {
        let objParam={
            dias: form.value.dias
        };
        this.httpService.setRuta('rest/consultas/vencimientos');
        this.httpService.add(objParam)
            .subscribe(e => {
                if(e['status']===1){
                    this.mensajeResponse=e['message'];
                    this.clientes=[];

                }
                else{
                    this.mensajeResponse='';
                    this.clientes=e['data']['clientes'];
                }

                },
                err => {
                    console.log(err);
                    Notificador.show(err);

                });
    }

    exportAsXLSX():void {
        //copiar datos a exportar
        let obj={};
        this.serviciosParaExcel=[];
        for( let i=0; i<this.clientes.length;i++){
            obj['clienteId']=this.clientes[i].clienteId;
            obj['cliente']=this.clientes[i].nombre + ' ' + this.clientes[i].apellido;
            obj['nroDocumento']=this.clientes[i].nroDocumento;
            obj['fechaCaducidad']=this.clientes[i].fechaCaducidad;
            this.serviciosParaExcel.push(obj);
            obj={};
        }


        this.excelService.exportAsExcelFile(this.serviciosParaExcel, 'Reporte-VencimientoClientes');
    }

    exportAsPDF(): void{

        var columns = [
            {title: "ID", dataKey: "clienteId"},
            {title: "Cliente", dataKey: "cliente"},
            {title: "Nro Documento", dataKey: "nroDocumento"},
            {title: "Fecha de Caducidad", dataKey: "fechaCaducidad"},

        ];


        //copiar datos a exportar
        let obj = {};
        this.serviciosParaPDF = [];

        for( let i=0; i<this.clientes.length;i++){
            obj['clienteId']=this.clientes[i].clienteId;
            obj['cliente']=this.clientes[i].nombre + ' ' + this.clientes[i].apellido;
            obj['nroDocumento']=this.clientes[i].nroDocumento;
            obj['fechaCaducidad']=this.clientes[i].fechaCaducidad;
            this.serviciosParaPDF.push(obj);
            obj={};
        }
        var doc = new jspdf('p', 'mm', 'legal');

        doc.autoTable(columns, this.serviciosParaPDF);
        doc.save('Reporte-VencimientoClientes.pdf');

    }

}
