import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MaterialModule} from '../../app.module';

import {ReportesRoutes} from './reportes.routing';
import {ReporteUsoPuntosComponent} from './reporte-usoPuntos/reporte-usoPuntos.component';
import {BuscadorConceptoUsoComponent} from '../dialogBuscador/buscador-conceptoUso/buscador-conceptoUso.component';
import {BuscadorClienteComponent} from '../dialogBuscador/buscador-cliente/buscador-cliente.component';
import {SharedModule} from '../../sharedModule';
import {MatSelectFilterModule} from 'mat-select-filter';
import {ConsultarClientesVencimientoComponent} from './reporte-clientesVencimiento/consultarClientesVencimiento.component';
import {ReporteBolsaPuntosComponent} from './reporte-bolsaPuntos/reporte-bolsaPuntos.component';


@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(ReportesRoutes),
        FormsModule,
        MaterialModule,
        ReactiveFormsModule,
        MatSelectFilterModule,

        SharedModule.forRoot(),
    ],
    declarations: [
        ReporteUsoPuntosComponent,
        ReporteBolsaPuntosComponent,
        ConsultarClientesVencimientoComponent
    ],
    entryComponents: [
        BuscadorConceptoUsoComponent,
        BuscadorClienteComponent
    ],
})

export class ReportesModule {
}
