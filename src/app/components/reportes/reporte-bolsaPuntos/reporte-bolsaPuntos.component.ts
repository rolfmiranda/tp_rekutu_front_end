import {Component, OnInit, ViewChild} from '@angular/core';
import {
    DateAdapter,
    MAT_DATE_FORMATS,
    MAT_DATE_LOCALE,
    MatDatepicker, MatDialog
} from '@angular/material';
import {FilteringService} from '../../../services/URLParamsHandlers/filtering-service';
import {MAT_MOMENT_DATE_FORMATS, MomentDateAdapter} from '@angular/material-moment-adapter';
import {HTTPService} from '../../../services/HTTP-service';
import {Notificador} from '../../../functions/notifications';
import {ExcelService} from '../../../services/excel-service';
import {BuscadorConceptoUsoComponent} from '../../dialogBuscador/buscador-conceptoUso/buscador-conceptoUso.component';
import {BuscadorClienteComponent} from '../../dialogBuscador/buscador-cliente/buscador-cliente.component';
import {BuscadorService} from '../../../services/buscador-service';

declare var $: any;
import * as jspdf from 'jspdf';
import 'jspdf-autotable';

@Component({
    selector: 'app-resumen',
    templateUrl: './reporte-bolsaPuntos.component.html',
    styleUrls:['reporte-bolsaPuntos.component.css'],
    providers: [
        {provide: MAT_DATE_LOCALE, useValue: 'es-PY'},
        {provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE]},
        {provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS}
    ]
})

export class ReporteBolsaPuntosComponent implements OnInit {
    dataSource=[];
    serviciosParaExcel=[];
    serviciosParaPDF=[];
    clienteSelected:any;

    constructor(private httpService: HTTPService,
                public filteringService: FilteringService,
                public buscadorService: BuscadorService,
                public dialog: MatDialog,
                private excelService: ExcelService) {
    }

    @ViewChild('fechaDesdePicker', {static: true}) fechaDesdeFilter: MatDatepicker<Date>;
    @ViewChild('fechaHastaPicker', {static: true}) fechaHastaFilter: MatDatepicker<Date>;

    ngOnInit() {
        this.clienteSelected='null';
        this.loadData({});

    }



    loadData(objParam:any) {
        this.httpService.setRuta('rest/consultas/bolsaPuntos');
        this.httpService.add(objParam)
            .subscribe(e => {
                    this.dataSource = e['data']['bolsaPuntos'];

                },
                err => {
                    console.log(err);
                    err['customMensaje'] = 'No se pudo obtener la lista de usos de puntos.';
                    Notificador.show(err);

                });
    }

    dialogFilterCliente() {
        this.buscadorService.setRequired(false);
        const dialogRef = this.dialog.open(BuscadorClienteComponent);
        dialogRef.afterClosed().subscribe(result => {
            if(result===true){
                this.clienteSelected=this.buscadorService.getCliente;
                this.buscadorService.reset();

                this.handleFilter();
            }
        });
    }
    limpiarFiltroCliente() {
        this.clienteSelected = 'null';
        this.handleFilter();
    }

    handleFilter() {
        let objParam={};
        if(this.clienteSelected !=='null'){
            objParam['clienteId']=this.clienteSelected.id;
        }
        this.loadData(objParam);
    }


    exportAsXLSX():void {
        //copiar datos a exportar
        let obj={};
        this.serviciosParaExcel=[];
            for( let i=0; i<this.dataSource.length;i++){
                obj['idBolsa']=this.dataSource[i].id;
                obj['cliente']=this.dataSource[i].cliente.nombre + ' ' + this.dataSource[i].cliente.apellido;
                obj['estado']=this.dataSource[i].estado;
                obj['fechaAsignacion']=this.dataSource[i].fechaAsignacion;
                obj['fechaCaducidad']=this.dataSource[i].fechaCaducidad;
                obj['monto'] = this.dataSource[i].monto;
                obj['puntajeAsignado'] = this.dataSource[i].puntajeAsignado;
                obj['puntajeUtilizado'] = this.dataSource[i].puntajeUtilizado;
                obj['saldo'] = this.dataSource[i].saldo;
                this.serviciosParaExcel.push(obj);
                obj={};
            }


        this.excelService.exportAsExcelFile(this.serviciosParaExcel, 'Reporte-BolsaPuntos');
    }

    exportAsPDF(): void{

        var columns = [
            {title: "ID", dataKey: "idBolsa"},
            {title: "Cliente", dataKey: "cliente"},
            {title: "Estado", dataKey: "estado"},
            {title: "Fecha de Asignacion", dataKey: "fechaAsignacion"},
            {title: "Fecha de Caducidad", dataKey: "fechaCaducidad"},
            {title: "Monto", dataKey: "monto"},
            {title: "Puntaje Asignado", dataKey: "puntajeAsignado"},
            {title: "Puntaje Utilizado", dataKey: "puntajeUtilizado"},
            {title: "Saldo", dataKey: "saldo"},

        ];


        //copiar datos a exportar
        let obj = {};
        this.serviciosParaPDF = [];

        for( let i=0; i<this.dataSource.length;i++){
            obj['idBolsa']=this.dataSource[i].id;
            obj['cliente']=this.dataSource[i].cliente.nombre + ' ' + this.dataSource[i].cliente.apellido;
            obj['estado']=this.dataSource[i].estado;
            obj['fechaAsignacion']=this.dataSource[i].fechaAsignacion;
            obj['fechaCaducidad']=this.dataSource[i].fechaCaducidad;
            obj['monto'] = this.dataSource[i].monto;
            obj['puntajeAsignado'] = this.dataSource[i].puntajeAsignado;
            obj['puntajeUtilizado'] = this.dataSource[i].puntajeUtilizado;
            obj['saldo'] = this.dataSource[i].saldo;
            this.serviciosParaPDF.push(obj);
            obj={};
        }
        var doc = new jspdf('l', 'mm', 'legal');

        doc.autoTable(columns, this.serviciosParaPDF);
        doc.save('Reporte-UsoPuntos.pdf');

    }
}


