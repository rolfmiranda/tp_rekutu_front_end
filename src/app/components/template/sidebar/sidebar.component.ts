import { Component, OnInit } from '@angular/core';
import PerfectScrollbar from 'perfect-scrollbar';

declare const $: any;

//Metadata
export interface RouteInfo {
    path: string;
    title: string;
    type: string;
    icontype: string;
    collapse?: string;
    children?: ChildrenItems[];
}

export interface ChildrenItems {
    path: string;
    title: string;
    ab: string;
    type?: string;
}

//Menu Items
export const ROUTES: RouteInfo[] = [
    {
        path: '/clientes/',
        title: 'Clientes',
        type: 'sub',
        icontype: 'people',
        collapse: 'clientes',
        children: [
            {path: 'lista', title: 'Lista de Clientes', ab:'LC'},
            {path: 'cargar_puntos', title: 'Cargar Puntos', ab:'CP'},
            {path: 'usar_puntos', title: 'Registrar Uso de Puntos', ab:'UP'},
        ]
    },
    {
        path: '/puntos/',
        title: 'Puntos',
        type: 'sub',
        icontype: 'pie_chart',
        collapse: 'puntos',
        children: [
            {path: 'conceptos_uso', title: 'Conceptos de Uso', ab:'CU'},
            {path: 'reglas_asignacion', title: 'Reglas de Asignacion', ab:'RA'},
            {path: 'parametros_vencimiento', title: 'Parametros de Vencimiento', ab:'PV'},
        ]
    },
    {
        path: '/reportes/',
        title: 'Reportes',
        type: 'sub',
        icontype: 'info',
        collapse: 'reportes',
        children: [
            {path: 'puntos_uso', title: 'Uso de puntos', ab:'UP'},
            {path: 'bolsas_puntos', title: 'Bolsas de Puntos', ab:'BP'},
            {path: 'clientes_vencimiento', title: 'Vencimiento de Puntos de Clientes', ab:'VPC'},
        ]
    }
];
@Component({
    selector: 'app-sidebar-cmp',
    templateUrl: 'sidebar.component.html',
})

export class SidebarComponent implements OnInit {
    usuarioActual= JSON.parse(localStorage.getItem('currentUser'));
    public menuItems: any[];
    ps: any;
    isMobileMenu() {
        if ($(window).width() > 991) {
            return false;
        }
        return true;
    };

    ngOnInit() {
        this.menuItems = ROUTES.filter(menuItem => menuItem);
        if (window.matchMedia(`(min-width: 960px)`).matches && !this.isMac()) {
            const elemSidebar = <HTMLElement>document.querySelector('.sidebar .sidebar-wrapper');
            this.ps = new PerfectScrollbar(elemSidebar);
        }
    }
    updatePS(): void  {
        if (window.matchMedia(`(min-width: 960px)`).matches && !this.isMac()) {
            this.ps.update();
        }
    }
    isMac(): boolean {
        let bool = false;
        if (navigator.platform.toUpperCase().indexOf('MAC') >= 0 || navigator.platform.toUpperCase().indexOf('IPAD') >= 0) {
            bool = true;
        }
        return bool;
    }
}
