import {Component, OnInit, QueryList, ViewChild, ViewChildren} from '@angular/core';
import {MatDialog, MatPaginator, MatTableDataSource, PageEvent} from '@angular/material';
import {Notificador} from '../../../functions/notifications';
import {HTTPService} from '../../../services/HTTP-service';
import {PaginationService} from '../../../services/URLParamsHandlers/pagination-service';
import {SortingService} from '../../../services/URLParamsHandlers/sorting-service';
import {FilteringService} from '../../../services/URLParamsHandlers/filtering-service';
import {SelectionModel} from '@angular/cdk/collections';
import {BuscadorService} from '../../../services/buscador-service';

@Component({
    selector: 'buscador-cliente',
    templateUrl: 'buscador-cliente.html',
})
export class BuscadorClienteComponent  implements  OnInit{
    dataSource =[];
    displayedColumns = ['select','nombre', 'apellido'];

    selection = new SelectionModel<any>(false, []);

    constructor(public httpService: HTTPService,
                public paginationService: PaginationService,
                public filteringService: FilteringService,
                public buscadorService: BuscadorService ) {
    }


    @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
    nombreSelected = '';

    ngOnInit(): void {
        this.getClientes({
                startIndexPage: this.paginationService.page* this.paginationService.pageSize,
                pageSize: this.paginationService.pageSize
            },
            {
                UrlParams: ['pagination'],
                resetParams: ['pagination']
            });
    }

    getClientes( pagination: any,options?: any) {
        this.httpService.setRuta('rest/clientes/getByPage?parametro='+this.nombreSelected);
        this.httpService.add(pagination)
            .subscribe(e => {
                    this.dataSource = e['data']['clientes'];
                },
                err => {
                    console.log(err);
                    err['customMensaje'] = 'No se pudo obtener la lista de clientes.';

                    Notificador.show(err);
                });
    }

    handleFilter() {
        this.getClientes({
                startIndexPage: this.paginationService.page* this.paginationService.pageSize,
                pageSize: this.paginationService.pageSize
            },
            {
                UrlParams: ['pagination'],
                resetParams: ['pagination']
            });
    }

    goAnterior() {
        this.paginationService.goTo('anterior');
        this.getClientes({
                startIndexPage: this.paginationService.page* this.paginationService.pageSize,
                pageSize: this.paginationService.pageSize
            },
            {
                UrlParams: ['pagination']
            });
    }
    goSiguiente() {
        this.paginationService.goTo('siguiente');
        this.getClientes({
                startIndexPage: this.paginationService.page * this.paginationService.pageSize,
                pageSize: this.paginationService.pageSize
            },
            {
                UrlParams: ['pagination']
            });
    }

    goFirst() {
        this.paginationService.reset();
        this.getClientes({
                startIndexPage: this.paginationService.page* this.paginationService.pageSize,
                pageSize: this.paginationService.pageSize
            },
            {
                UrlParams: ['pagination']
            });
    }

    /** Whether the number of selected elements matches the total number of rows. */
    isAllSelected() {
        const numSelected = this.selection.selected.length;
        const numRows = this.dataSource.length;
        return numSelected === numRows;
    }

    /** Selects all rows if they are not all selected; otherwise clear selection. */
    masterToggle() {
        this.isAllSelected() ?
            this.selection.clear() :
            this.dataSource.forEach(row => this.selection.select(row));
    }

    /** The label for the checkbox on the passed row */
    checkboxLabel(row?: any): string {
        if (!row) {
            return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
        }
        return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.position + 1}`;
    }

    setCliente() {
        this.buscadorService.setCliente(this.selection.selected[0]);
    }

    limpiarFiltroNombre() {
        this.nombreSelected='';
        this.getClientes({
                startIndexPage: this.paginationService.page* this.paginationService.pageSize,
                pageSize: this.paginationService.pageSize
            },
            {
                UrlParams: ['pagination'],
                resetParams: ['pagination']
            });
    }

}
