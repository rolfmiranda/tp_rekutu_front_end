import {Component, OnInit, QueryList, ViewChild, ViewChildren} from '@angular/core';
import {MatDialog, MatPaginator, MatTableDataSource, PageEvent} from '@angular/material';
import {Notificador} from '../../../functions/notifications';
import {HTTPService} from '../../../services/HTTP-service';
import {PaginationService} from '../../../services/URLParamsHandlers/pagination-service';
import {SortingService} from '../../../services/URLParamsHandlers/sorting-service';
import {FilteringService} from '../../../services/URLParamsHandlers/filtering-service';
import {SelectionModel} from '@angular/cdk/collections';
import {BuscadorService} from '../../../services/buscador-service';

@Component({
    selector: 'buscador-empleado',
    templateUrl: 'buscador-conceptoUso.html',
})
export class BuscadorConceptoUsoComponent  implements  OnInit{
    descripcionConceptoUso='';
    dataSource = [];
    totalDatos=0;
    displayedColumns = ['select','descripcion','cantidadRequerida'];

    selection = new SelectionModel<any>(false, []);

    constructor(public httpService: HTTPService,
                public paginationService: PaginationService,
                public filteringService: FilteringService,
                public buscadorService: BuscadorService ) {
    }


    @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
    ngOnInit(): void {
        this.getConceptosUso(
            {
                UrlParams: ['pagination'],
                resetParams: ['pagination']
            });
    }

    getConceptosUso(options?: any) {
        this.httpService.setRuta('rest/vales/paginado?page='+this.paginationService.page+'&size='+this.paginationService.pageSize);
        this.httpService.getAll(options)
            .subscribe(e => {
                    this.dataSource = e['data']['vales'];
                },
                err => {
                    console.log(err);
                    err['customMensaje'] = 'No se pudo obtener la lista de conceptos de uso.';

                    Notificador.show(err);
                });
    }

    handleFilter() {
        this.getConceptosUso(
            {
                UrlParams: ['pagination'],
                resetParams: ['pagination']
            });
    }

    goAnterior() {
        this.paginationService.goTo('anterior');
        this.getConceptosUso(
            {
                UrlParams: ['pagination']
            });
    }
    goSiguiente() {
        this.paginationService.goTo('siguiente');
        this.getConceptosUso(
            {
                UrlParams: ['pagination']
            });
    }

    goFirst() {
        this.paginationService.reset();
        this.getConceptosUso(
            {
                UrlParams: ['pagination']
            });
    }
    /** Whether the number of selected elements matches the total number of rows. */
    isAllSelected() {
        const numSelected = this.selection.selected.length;
        const numRows = this.dataSource.length;
        return numSelected === numRows;
    }

    /** Selects all rows if they are not all selected; otherwise clear selection. */
    masterToggle() {
        this.isAllSelected() ?
            this.selection.clear() :
            this.dataSource.forEach(row => this.selection.select(row));
    }

    /** The label for the checkbox on the passed row */
    checkboxLabel(row?: any): string {
        if (!row) {
            return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
        }
        //console.log(this.selection);
        return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.position + 1}`;
    }

    setConceptoUso() {
        this.buscadorService.setConceptoUso(this.selection.selected[0]);
    }


    limpiarFiltroDescripcion() {
        this.descripcionConceptoUso='';
        this.handleFilter();
    }

}
