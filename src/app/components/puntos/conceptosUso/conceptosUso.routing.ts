import {Routes} from '@angular/router';

import {ListaConceptosUsoComponent} from './lista-conceptosUso/lista-conceptosUso.component';
import {NuevoConceptoUsoComponent} from './nuevo-conceptoUso/nuevo-conceptoUso.component';


export const ConceptosUsoRoutes: Routes = [
    {
        path: '',
        children: [
            {
                path: '',
                redirectTo: 'lista'
            },
            {
                path: 'lista',
                component: ListaConceptosUsoComponent
            },
            {
                path: 'nuevo',
                component: NuevoConceptoUsoComponent
            }
        ]
    }
];
