import {Component, OnInit} from '@angular/core';

import {PaginationService} from '../../../../services/URLParamsHandlers/pagination-service';
import {HTTPService} from '../../../../services/HTTP-service';

import {Notificador} from '../../../../functions/notifications';


declare var $: any;

@Component({
    selector: 'app-pacientes',
    templateUrl: './lista-conceptosUso.component.html'
})

export class ListaConceptosUsoComponent implements OnInit {
    dataSource = [];
    constructor(public httpService: HTTPService,
                public paginationService: PaginationService) {
    }


    ngOnInit() {
        this.paginationService.reset();
        this.loadData(
            {
                UrlParams: ['pagination'],
                resetParams: ['pagination']
            });
    }


    loadData(options?: any) {
        this.httpService.setRuta('rest/vales/paginado?page='+this.paginationService.page+'&size='+this.paginationService.pageSize);
        this.httpService.getAll(options)
            .subscribe(e => {
                console.log('rest/vales/paginado?page='+this.paginationService.page+'&size='+this.paginationService.pageSize);
                console.log(e);
                    this.dataSource = e['data']['vales'];
                },
                err => {
                    console.log(err);
                    err['customMensaje'] = 'No se pudo obtener la lista de Conceptos.';

                    Notificador.show(err);
                });
    }


    handleFilter() {
        this.paginationService.reset();
        this.loadData(
            {
                UrlParams: ['pagination']
            });
    }

    goAnterior() {
        this.paginationService.goTo('anterior');
        this.loadData({
            UrlParams: ['pagination']
        });
    }
    goSiguiente() {
        this.paginationService.goTo('siguiente');
        this.loadData({
            UrlParams: ['pagination']
        });
    }

    goFirst() {
        this.paginationService.reset();
        this.loadData(
            {
                UrlParams: ['pagination']
            });
    }
}


