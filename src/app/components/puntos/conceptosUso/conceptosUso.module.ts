import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {MaterialModule} from '../../../app.module';

import {MatSelectFilterModule} from 'mat-select-filter';
import {ConceptosUsoRoutes} from './conceptosUso.routing';
import {ListaConceptosUsoComponent} from './lista-conceptosUso/lista-conceptosUso.component';
import {NuevoConceptoUsoComponent} from './nuevo-conceptoUso/nuevo-conceptoUso.component';


@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(ConceptosUsoRoutes),
        FormsModule,
        MaterialModule,
        MatSelectFilterModule
    ],
    declarations: [
        ListaConceptosUsoComponent,
        NuevoConceptoUsoComponent
    ]
})

export class ConceptosUsoModule {
}
