import {Routes} from '@angular/router';

import {ListaParametrosVencimientoPuntosComponent} from './lista-parametrosVencimientoPuntos/lista-parametrosVencimientoPuntos.component';
import {NuevoAsignacionPuntosComponent} from './nuevo-parametrosVencimientoPuntos/nuevo-asignacionPuntos.component';
import {EditarAsignacionPuntosComponent} from './editar-parametrosVencimientoPuntos/editar-asignacionPuntos.component';


export const ParametrosVencimientoPuntosRoutes: Routes = [
    {
        path: '',
        children: [
            {
                path: '',
                redirectTo: 'lista'
            },
            {
                path: 'lista',
                component: ListaParametrosVencimientoPuntosComponent
            },
            {
                path: 'nuevo',
                component: NuevoAsignacionPuntosComponent
            },
            {
                path: 'editar/:id',
                component: EditarAsignacionPuntosComponent
            }
        ]
    }
];
