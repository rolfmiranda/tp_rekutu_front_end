import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {MaterialModule} from '../../../app.module';

import {MatSelectFilterModule} from 'mat-select-filter';
import {ParametrosVencimientoPuntosRoutes} from './parametrosVencimientoPuntos.routing';
import {ListaParametrosVencimientoPuntosComponent} from './lista-parametrosVencimientoPuntos/lista-parametrosVencimientoPuntos.component';
import {NuevoAsignacionPuntosComponent} from './nuevo-parametrosVencimientoPuntos/nuevo-asignacionPuntos.component';
import {EditarAsignacionPuntosComponent} from './editar-parametrosVencimientoPuntos/editar-asignacionPuntos.component';


@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(ParametrosVencimientoPuntosRoutes),
        FormsModule,
        MaterialModule,
        MatSelectFilterModule
    ],
    declarations: [
        ListaParametrosVencimientoPuntosComponent,
        NuevoAsignacionPuntosComponent,
        EditarAsignacionPuntosComponent
    ]
})

export class ParametrosVencimientoPuntosModule {
}
