import {Component} from '@angular/core';
import {NgForm} from '@angular/forms';
import {Location} from '@angular/common';
import {HTTPService} from '../../../../services/HTTP-service';
import {Notificador} from '../../../../functions/notifications';
import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE} from '@angular/material';
import {MAT_MOMENT_DATE_FORMATS, MomentDateAdapter} from '@angular/material-moment-adapter';
import * as moment from 'moment';

@Component({
    selector: 'app-nuevo-paciente',
    templateUrl: 'nuevo-asignacionPuntos.component.html',
    providers: [
        {provide: MAT_DATE_LOCALE, useValue: 'es-PY'},
        {provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE]},
        {provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS}
    ]
})

export class NuevoAsignacionPuntosComponent {
    constructor(public httpService: HTTPService,
                public location: Location
    ) {
    }

    onSubmit(form: NgForm) {
        let body = {};

        body['fechaInicioValidez'] = moment(form.value.fechaInicioValidez).format('YYYY-MM-DD');
        body['fechaFinValidez'] = moment(form.value.fechaFinValidez).format('YYYY-MM-DD');
        body['diasDuracion'] = form.value.diasDuracion;

        this.httpService.setRuta('rest/parametros/add');
        this.httpService.add(body)
            .subscribe(e => {
                    e['ok'] = true;
                    e['customMensaje'] = 'Parametro de Vencimiento de puntos creado exitosamente';
                    Notificador.show(e);
                    this.location.back();
                },
                err => {
                    console.log(err);
                    Notificador.show(err);

                });
    }
}
