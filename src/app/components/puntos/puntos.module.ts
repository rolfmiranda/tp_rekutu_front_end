import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {MaterialModule} from '../../app.module';

import {MatSelectFilterModule} from 'mat-select-filter';
import {PuntosRoutes} from './puntos.routing';
import {BuscadorClienteComponent} from '../dialogBuscador/buscador-cliente/buscador-cliente.component';
import {SharedModule} from '../../sharedModule';
import {BuscadorConceptoUsoComponent} from '../dialogBuscador/buscador-conceptoUso/buscador-conceptoUso.component';


@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(PuntosRoutes),
        FormsModule,
        MaterialModule,
        MatSelectFilterModule,
        SharedModule.forRoot(),


    ],
    declarations: [

    ],
    entryComponents: [
        BuscadorClienteComponent,
        BuscadorConceptoUsoComponent
    ],

})

export class PuntosModule {
}
