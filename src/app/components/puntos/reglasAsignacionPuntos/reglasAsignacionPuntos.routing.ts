import {Routes} from '@angular/router';

import {ListaAsignacionPuntosComponent} from './lista-reglasAsignacionPuntos/lista-asignacionPuntos.component';
import {NuevoAsignacionPuntosComponent} from './nuevo-reglasAsignacionPuntos/nuevo-asignacionPuntos.component';
import {EditarAsignacionPuntosComponent} from './editar-reglasAsignacionPuntos/editar-asignacionPuntos.component';


export const ReglasAsignacionPuntosRoutes: Routes = [
    {
        path: '',
        children: [
            {
                path: '',
                redirectTo: 'lista'
            },
            {
                path: 'lista',
                component: ListaAsignacionPuntosComponent
            },
            {
                path: 'nuevo',
                component: NuevoAsignacionPuntosComponent
            },
            {
                path: 'editar/:id',
                component: EditarAsignacionPuntosComponent
            }
        ]
    }
];
