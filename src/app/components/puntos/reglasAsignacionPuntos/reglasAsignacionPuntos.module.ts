import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {MaterialModule} from '../../../app.module';

import {MatSelectFilterModule} from 'mat-select-filter';
import {ReglasAsignacionPuntosRoutes} from './reglasAsignacionPuntos.routing';
import {ListaAsignacionPuntosComponent} from './lista-reglasAsignacionPuntos/lista-asignacionPuntos.component';
import {NuevoAsignacionPuntosComponent} from './nuevo-reglasAsignacionPuntos/nuevo-asignacionPuntos.component';
import {EditarAsignacionPuntosComponent} from './editar-reglasAsignacionPuntos/editar-asignacionPuntos.component';


@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(ReglasAsignacionPuntosRoutes),
        FormsModule,
        MaterialModule,
        MatSelectFilterModule
    ],
    declarations: [
        ListaAsignacionPuntosComponent,
        NuevoAsignacionPuntosComponent,
        EditarAsignacionPuntosComponent
    ]
})

export class ReglasAsignacionPuntosModule {
}
