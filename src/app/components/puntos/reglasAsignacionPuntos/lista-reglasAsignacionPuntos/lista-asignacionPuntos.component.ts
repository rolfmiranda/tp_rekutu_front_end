import {Component, OnInit} from '@angular/core';

import {PaginationService} from '../../../../services/URLParamsHandlers/pagination-service';
import {HTTPService} from '../../../../services/HTTP-service';

import {Notificador} from '../../../../functions/notifications';

@Component({
    selector: 'app-pacientes',
    templateUrl: './lista-asignacionPuntos.component.html'
})

export class ListaAsignacionPuntosComponent implements OnInit {
    dataSource = [];
    constructor(public httpService: HTTPService,
                public paginationService: PaginationService) {
    }


    ngOnInit() {
        this.paginationService.reset();
        this.loadData(
            {
                UrlParams: ['pagination'],
                resetParams: ['pagination']
            });
    }


    loadData( options?: any) {
        this.httpService.setRuta('rest/reglas/all');
        this.httpService.getAll()
            .subscribe(e => {
                    this.dataSource = e['data']['reglas'];
                },
                err => {
                    console.log(err);
                    err['customMensaje'] = 'No se pudo obtener la lista de Reglas para Asignacion de Puntos.';

                    Notificador.show(err);
                });
    }

}


