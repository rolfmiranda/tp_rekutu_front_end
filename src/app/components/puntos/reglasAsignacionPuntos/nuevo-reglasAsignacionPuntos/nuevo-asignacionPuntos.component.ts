import {Component} from '@angular/core';
import {NgForm} from '@angular/forms';
import {Location} from '@angular/common';
import {HTTPService} from '../../../../services/HTTP-service';
import {Notificador} from '../../../../functions/notifications';
import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE} from '@angular/material';
import {MAT_MOMENT_DATE_FORMATS, MomentDateAdapter} from '@angular/material-moment-adapter';

@Component({
    selector: 'app-nuevo-paciente',
    templateUrl: 'nuevo-asignacionPuntos.component.html',
    providers: [
        {provide: MAT_DATE_LOCALE, useValue: 'es-PY'},
        {provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE]},
        {provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS}
    ]
})

export class NuevoAsignacionPuntosComponent {
    constructor(public httpService: HTTPService,
                public location: Location
    ) {
    }

    onSubmit(form: NgForm) {
        let body = {};

        body['montoEquivalencia'] = form.value.montoEquivalencia;
        if (form.value.limiteInferior) {
            body['limiteInferior'] = form.value.limiteInferior;
        }
        if (form.value.limiteSuperior) {
            body['limiteSuperior'] = form.value.limiteSuperior;
        }

        this.httpService.setRuta('rest/reglas/add');
        this.httpService.add(body)
            .subscribe(e => {
                    e['ok'] = true;
                    e['customMensaje'] = 'Regla de asignacion de puntos creada exitosamente';
                    Notificador.show(e);
                    this.location.back();
                },
                err => {
                    console.log(err);
                    Notificador.show(err);

                });
    }
}
