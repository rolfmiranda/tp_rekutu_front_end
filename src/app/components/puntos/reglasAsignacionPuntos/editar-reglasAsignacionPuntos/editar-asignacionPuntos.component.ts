import {Component, OnInit} from '@angular/core';
import {NgForm} from '@angular/forms';
import {Location} from '@angular/common';
import {HTTPService} from '../../../../services/HTTP-service';
import {Notificador} from '../../../../functions/notifications';
import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE} from '@angular/material';
import {MAT_MOMENT_DATE_FORMATS, MomentDateAdapter} from '@angular/material-moment-adapter';
import {ActivatedRoute} from '@angular/router';

@Component({
    selector: 'app-nuevo-paciente',
    templateUrl: 'editar-asignacionPuntos.component.html',
    providers: [
        {provide: MAT_DATE_LOCALE, useValue: 'es-PY'},
        {provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE]},
        {provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS}
    ]
})

export class EditarAsignacionPuntosComponent implements OnInit{
    private idReglaAsignacion = this.route.snapshot.paramMap.get('id');
    private reglaAsignacion: any={};


    constructor(public httpService: HTTPService,
                public location: Location,
                public route: ActivatedRoute,
    ) {
    }

    ngOnInit(): void {
        this.getReglaAsignacion()
    }
    getReglaAsignacion( options?: any) {
        this.httpService.setRuta('rest/reglas/id/'+this.idReglaAsignacion);
        this.httpService.getAll()
            .subscribe(e => {
                    this.reglaAsignacion = e;
                },
                err => {
                    console.log(err);
                    err['customMensaje'] = 'No se pudo obtener la regla de Asignacion de Puntos.';

                    Notificador.show(err);
                });
    }
    onSubmit(form: NgForm) {
        let body = {};

        body['montoEquivalencia'] = form.value.montoEquivalencia;
        if (form.value.limiteInferior) {
            body['limiteInferior'] = form.value.limiteInferior;
        }
        if (form.value.limiteSuperior) {
            body['limiteSuperior'] = form.value.limiteSuperior;
        }

        this.httpService.setRuta('rest/reglas/edit/'+this.idReglaAsignacion);
        this.httpService.update(body)
            .subscribe(e => {
                    e['ok'] = true;
                    e['customMensaje'] = 'Regla de asignacion de puntos actualizada exitosamente';
                    Notificador.show(e);
                    this.location.back();
                },
                err => {
                    console.log(err);
                    Notificador.show(err);

                });
    }
}
