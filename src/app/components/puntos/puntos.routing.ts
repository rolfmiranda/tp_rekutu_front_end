import {Routes} from '@angular/router';

import {NuevoCargarPuntosComponent} from '../clientes/cargarPuntos/nuevo-cargarPuntos.component';
import {NuevoUsarPuntosComponent} from '../clientes/usarPuntos/nuevo-usarPuntos.component';
import {ListaConceptosUsoComponent} from './conceptosUso/lista-conceptosUso/lista-conceptosUso.component';


export const PuntosRoutes: Routes = [
    {
        path: '',
        children: [
            {
                path: '',
                redirectTo: 'conceptos_uso'
            },
            {
                path: 'conceptos_uso',
                loadChildren: './conceptosUso/conceptosUso.module#ConceptosUsoModule'

            },
            {
                path: 'parametros_vencimiento',
                loadChildren: './parametrosVencimientoPuntos/parametrosVencimientoPuntos.module#ParametrosVencimientoPuntosModule'

            },
            {
                path: 'reglas_asignacion',
                loadChildren: './reglasAsignacionPuntos/reglasAsignacionPuntos.module#ReglasAsignacionPuntosModule'

            },
        ]
    }
];
