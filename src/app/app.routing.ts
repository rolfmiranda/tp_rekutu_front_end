import {Routes} from '@angular/router';

import {AdminLayoutComponent} from './components/template/layouts/admin/admin-layout.component';

export const AppRoutes: Routes = [
    {
        path: '',
        redirectTo: 'clientes',
        pathMatch: 'full',
    },
    {
        path: '',
        component: AdminLayoutComponent,
        children: [
            {
                path: '',
                redirectTo: 'clientes',
                pathMatch: 'full',
            },

            {
                path: 'clientes',
                loadChildren: './components/clientes/clientes.module#ClientesModule'
            },
            {
                path: 'puntos',
                loadChildren: './components/puntos/puntos.module#PuntosModule'
            },
            {
                path: 'reportes',
                loadChildren: './components/reportes/reportes.module#ReportesModule'
            },

        ]
    }
];
