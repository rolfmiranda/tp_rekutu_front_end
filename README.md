
## Instalación del sistema


1. Instalar Node desde [Página Oficial de Node.js](https://nodejs.org/es/).
2. Abrir la terminal
3. Acceder al directorio del proyecto
4. Si no tienes Angular-Cli, instalar con: ```npm install -g @angular/cli```
5. Luego: ```npm install```
6. y: ```npm start```
7. Se abrirá automáticaente el navegador en [localhost:4200](localhost:4200)

## Navegadores soportados

Hasta el momento, oficialmente es compatible con los siguientes navegadores:

<img src="https://s3.amazonaws.com/creativetim_bucket/github/browser/chrome.png" width="64" height="64"> <img src="https://s3.amazonaws.com/creativetim_bucket/github/browser/firefox.png" width="64" height="64"> <img src="https://s3.amazonaws.com/creativetim_bucket/github/browser/edge.png" width="64" height="64"> <img src="https://s3.amazonaws.com/creativetim_bucket/github/browser/safari.png" width="64" height="64"> <img src="https://s3.amazonaws.com/creativetim_bucket/github/browser/opera.png" width="64" height="64">

## Integrantes
1. Blas Miranda.
2. Myriam Godoy.

